/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */
#ifndef ATMEL_START_PINS_H_INCLUDED
#define ATMEL_START_PINS_H_INCLUDED

#include <hal_gpio.h>

// SAMC21 has 9 pin functions

#define GPIO_PIN_FUNCTION_A 0
#define GPIO_PIN_FUNCTION_B 1
#define GPIO_PIN_FUNCTION_C 2
#define GPIO_PIN_FUNCTION_D 3
#define GPIO_PIN_FUNCTION_E 4
#define GPIO_PIN_FUNCTION_F 5
#define GPIO_PIN_FUNCTION_G 6
#define GPIO_PIN_FUNCTION_H 7
#define GPIO_PIN_FUNCTION_I 8

#define BPP_POT GPIO(GPIO_PORTA, 8)
#define APP_ENA GPIO(GPIO_PORTA, 22)
#define PA24 GPIO(GPIO_PORTA, 24)
#define PA25 GPIO(GPIO_PORTA, 25)
#define STEER_ENA GPIO(GPIO_PORTA, 28)
#define APP_POT GPIO(GPIO_PORTB, 9)
#define APP_PULSE GPIO(GPIO_PORTB, 12)
#define APP_DIR GPIO(GPIO_PORTB, 13)
#define SW_0 GPIO(GPIO_PORTB, 19)
#define STEER_PULSE GPIO(GPIO_PORTB, 22)
#define STEER_DIR GPIO(GPIO_PORTB, 23)
#define BPP_PULSE GPIO(GPIO_PORTB, 30)
#define BPP_DIR GPIO(GPIO_PORTB, 31)
#define LED_0 GPIO(GPIO_PORTC, 5)
#define BPP_ENA GPIO(GPIO_PORTC, 24)

#endif // ATMEL_START_PINS_H_INCLUDED
