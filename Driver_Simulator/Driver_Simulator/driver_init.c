/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <peripheral_clk_config.h>
#include <utils.h>
#include <hal_init.h>

#include <hpl_adc_base.h>
#include <hpl_adc_base.h>

/* The channel amount for ADC */
#define ADC_APP_CH_AMOUNT 1

/* The buffer size for ADC */
#define ADC_APP_BUFFER_SIZE 16

/* The maximal channel number of enabled channels */
#define ADC_APP_CH_MAX 0

/* The channel amount for ADC */
#define ADC_BPP_CH_AMOUNT 1

/* The buffer size for ADC */
#define ADC_BPP_BUFFER_SIZE 16

/* The maximal channel number of enabled channels */
#define ADC_BPP_CH_MAX 0

struct adc_async_descriptor         ADC_APP;
struct adc_async_channel_descriptor ADC_APP_ch[ADC_APP_CH_AMOUNT];
struct adc_async_descriptor         ADC_BPP;
struct adc_async_channel_descriptor ADC_BPP_ch[ADC_BPP_CH_AMOUNT];
struct can_async_descriptor         CAN_0;

static uint8_t ADC_APP_buffer[ADC_APP_BUFFER_SIZE];
static uint8_t ADC_APP_map[ADC_APP_CH_MAX + 1];
static uint8_t ADC_BPP_buffer[ADC_BPP_BUFFER_SIZE];
static uint8_t ADC_BPP_map[ADC_BPP_CH_MAX + 1];

/**
 * \brief ADC initialization function
 *
 * Enables ADC peripheral, clocks and initializes ADC driver
 */
void ADC_APP_init(void)
{
	hri_mclk_set_APBCMASK_ADC0_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, ADC0_GCLK_ID, CONF_GCLK_ADC0_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	adc_async_init(
	    &ADC_APP, ADC0, ADC_APP_map, ADC_APP_CH_MAX, ADC_APP_CH_AMOUNT, &ADC_APP_ch[0], _adc_get_adc_async());
	adc_async_register_channel_buffer(&ADC_APP, 0, ADC_APP_buffer, ADC_APP_BUFFER_SIZE);

	// Disable digital pin circuitry
	gpio_set_pin_direction(APP_POT, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(APP_POT, PINMUX_PB09B_ADC0_AIN3);
}

/**
 * \brief ADC initialization function
 *
 * Enables ADC peripheral, clocks and initializes ADC driver
 */
void ADC_BPP_init(void)
{
	hri_mclk_set_APBCMASK_ADC1_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, ADC1_GCLK_ID, CONF_GCLK_ADC1_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	adc_async_init(
	    &ADC_BPP, ADC1, ADC_BPP_map, ADC_BPP_CH_MAX, ADC_BPP_CH_AMOUNT, &ADC_BPP_ch[0], _adc_get_adc_async());
	adc_async_register_channel_buffer(&ADC_BPP, 0, ADC_BPP_buffer, ADC_BPP_BUFFER_SIZE);

	// Disable digital pin circuitry
	gpio_set_pin_direction(BPP_POT, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(BPP_POT, PINMUX_PA08B_ADC1_AIN10);
}

void delay_driver_init(void)
{
	delay_init(SysTick);
}

void CAN_0_PORT_init(void)
{

	gpio_set_pin_function(PA25, PINMUX_PA25G_CAN0_RX);

	gpio_set_pin_function(PA24, PINMUX_PA24G_CAN0_TX);
}
/**
 * \brief CAN initialization function
 *
 * Enables CAN peripheral, clocks and initializes CAN driver
 */
void CAN_0_init(void)
{
	hri_mclk_set_AHBMASK_CAN0_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, CAN0_GCLK_ID, CONF_GCLK_CAN0_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	can_async_init(&CAN_0, CAN0);
	CAN_0_PORT_init();
}

void system_init(void)
{
	init_mcu();

	// GPIO on PA22

	gpio_set_pin_level(APP_ENA,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(APP_ENA, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(APP_ENA, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA28

	gpio_set_pin_level(STEER_ENA,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(STEER_ENA, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(STEER_ENA, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB12

	gpio_set_pin_level(APP_PULSE,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(APP_PULSE, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(APP_PULSE, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB13

	gpio_set_pin_level(APP_DIR,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(APP_DIR, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(APP_DIR, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB19

	// Set pin direction to input
	gpio_set_pin_direction(SW_0, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(SW_0,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_UP);

	gpio_set_pin_function(SW_0, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB22

	gpio_set_pin_level(STEER_PULSE,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(STEER_PULSE, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(STEER_PULSE, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB23

	gpio_set_pin_level(STEER_DIR,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(STEER_DIR, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(STEER_DIR, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB30

	gpio_set_pin_level(BPP_PULSE,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(BPP_PULSE, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(BPP_PULSE, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB31

	gpio_set_pin_level(BPP_DIR,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(BPP_DIR, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(BPP_DIR, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PC05

	gpio_set_pin_level(LED_0,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(LED_0, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(LED_0, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PC24

	gpio_set_pin_level(BPP_ENA,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(BPP_ENA, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(BPP_ENA, GPIO_PIN_FUNCTION_OFF);

	ADC_APP_init();
	ADC_BPP_init();

	delay_driver_init();
	CAN_0_init();
}
