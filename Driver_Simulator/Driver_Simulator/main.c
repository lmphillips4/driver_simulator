/*
 *
 *  main.c
 *
 */


#include <atmel_start.h>

#include "stepper_motor.h"
#include "pedal_pot.h"
#include "can_app.h"



int main(void)
{
	atmel_start_init();
	
	pedal_init();
	
	
	int32_t appDest = 0;
	int32_t bppDest = 0;
	
	bool SW_state = true;
	bool lastSW_state = true;

	while (1) {
		SW_state = gpio_get_pin_level(SW_0);
		
		if (SW_state != lastSW_state) {
			if (SW_state == true) {
				
				if (appDest == 0) {
					appDest = 1000;
					bppDest = 1000;
				}
				else {
					appDest = 0;
					bppDest = 0;
				}
				
				delay_ms(500);  //  this is to prevent debounce and double tapping
			}
		}
		
		pedal_set_destination_app(appDest);  // appDest range is 0-1000 (1000 is fully pressed)
		pedal_set_destination_bpp(bppDest);  // bppDest range is 0-1000 (1000 is fully pressed)
		
		pedal_pulse_handler();
	}
}




//////////////////////////////////////////////////////////////////////////
//
//  Below is code I'm working on, and is not used at all
//    best to not even look down here
//


//can_app_init();

//canMsg inputCmd;
//
//if (!can_app_get_msg(&inputCmd, 0)) {
	//appDest = (inputCmd.data[1]<<8) + inputCmd.data[0];
	//bppDest = (inputCmd.data[3]<<8) + inputCmd.data[2];
	//
	//if (appDest > 1000) appDest = 1000;
	//if (bppDest > 1000) bppDest = 1000;
//}
//
//if (loopCounter % 1000 == 0) {
	//uint8_t data[8];
	//data[0] = 0;
	//data[1] = 0;
	//data[2] = (uint8_t)(appDest & 0x000000FF);
	//data[3] = (uint8_t)((appDest & 0x0000FF00) >> 8);
	//data[4] = 0;
	//data[5] = 0;
	//data[6] = 0;
	//data[7] = 0;
	//can_app_send_msg(0x100, data, 8);
//}