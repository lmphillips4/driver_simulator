/*
 * can_app.c
 *
 * Created: 2/13/2019 4:24:10 PM
 *  Author: lphillips_admin
 */ 


#include "can_app.h"


static void can_app_set_filters (void);
static void can_app_rx_callback (struct can_async_descriptor *const descr);
static void can_app_tx_callback (struct can_async_descriptor *const descr);


#define RX_QUEUE_LENGTH  8

volatile canMsg can_Rx_Queue[RX_QUEUE_LENGTH];
volatile uint32_t can_Rx_Queue_head;
volatile uint32_t can_Rx_Queue_tail;




void can_app_init (void)
{
	can_Rx_Queue_head = 0;
	can_Rx_Queue_tail = 0;
	
	for (uint32_t i=0; i<RX_QUEUE_LENGTH; i++) {
		can_Rx_Queue[i].id = 0;
		for (uint32_t j=0; j<8; j++) can_Rx_Queue[i].data[j] = 0xFF;
	}
	
	can_async_register_callback(&CAN_0, CAN_ASYNC_TX_CB, (FUNC_PTR)can_app_tx_callback);
	can_async_register_callback(&CAN_0, CAN_ASYNC_RX_CB, (FUNC_PTR)can_app_rx_callback);
	can_async_enable(&CAN_0);
	
	can_app_set_filters();
	
	//timer_can_init();
}

uint8_t can_app_get_msg (canMsg *msgRx, uint16_t waitTime)
{
	uint8_t retVal = 0;
	uint16_t waitCounter = 0;
	
	while (can_Rx_Queue_head == can_Rx_Queue_tail) {
		if (waitCounter >= waitTime) {
			retVal = 1;
			return retVal;
		}
		waitCounter++;
		delay_us(100);
	}
	
	msgRx->id = can_Rx_Queue[can_Rx_Queue_head].id;
	
	for (uint32_t i=0; i<8; i++) msgRx->data[i] = can_Rx_Queue[can_Rx_Queue_head].data[i];
	
	can_Rx_Queue_head++;
	can_Rx_Queue_head %= RX_QUEUE_LENGTH;
	
	return retVal;
}

int32_t can_app_send_msg (uint32_t msgID, uint8_t *dataArray, uint8_t length)
{
	int32_t retVal = 0;
	
	struct can_message msgTx;
	uint8_t dataTx[8];
	
	for (uint32_t i=0; i<8; i++) {
		if (i < length) dataTx[i] = dataArray[i];
	}
	
	msgTx.id = msgID;
	msgTx.data = dataTx;
	msgTx.len = length;
	
	msgTx.fmt = CAN_FMT_STDID;
	msgTx.type = CAN_TYPE_DATA;
	
	retVal = can_async_write(&CAN_0, &msgTx);
	
	return retVal;
}

int32_t can_app_send_msg_ptr (canMsg message)
{
	int32_t retVal = 0;
	
	struct can_message msgTx;
	uint8_t dataTx[8];
	
	for (uint32_t i=0; i<8; i++) {
		dataTx[i] = message.data[i];
	}
	
	msgTx.id = message.id;
	msgTx.data = dataTx;
	msgTx.len = 8;
	
	msgTx.fmt = CAN_FMT_STDID;
	msgTx.type = CAN_TYPE_DATA;
	
	retVal = can_async_write(&CAN_0, &msgTx);
	
	return retVal;
}




//////////////////////////////////////////////////////////////////////////
//  Private Helpers


void can_app_set_filters (void)
{
	struct can_filter  filter;
	
	//  Filter 0
	filter.id   = CAN_FILTER_ID_0;
	filter.mask = 0;
	can_async_set_filter(&CAN_0, 0, CAN_FMT_STDID, &filter);
	
	//  Filter 1
	filter.id   = CAN_FILTER_ID_1;
	filter.mask = 0;
	can_async_set_filter(&CAN_0, 1, CAN_FMT_STDID, &filter);
	
}



void can_app_tx_callback (struct can_async_descriptor *const descr)
{
	(void)descr;
}

void can_app_rx_callback (struct can_async_descriptor *const descr)
{
	struct can_message msg;
	uint8_t            data[64];
	msg.data = data;
	can_async_read(descr, &msg);
	
	can_Rx_Queue[can_Rx_Queue_tail].id = msg.id;
	
	for (uint32_t i=0; i < 8; i++) {
		if (i < msg.len)  can_Rx_Queue[can_Rx_Queue_tail].data[i] = msg.data[i];
		else  can_Rx_Queue[can_Rx_Queue_tail].data[i] = 0xFF;
	}
	
	can_Rx_Queue_tail++;
	can_Rx_Queue_tail %= RX_QUEUE_LENGTH;
	
	if (can_Rx_Queue_tail == can_Rx_Queue_head) {
		can_Rx_Queue_head++;
		can_Rx_Queue_head %= RX_QUEUE_LENGTH;
	}
	
	// lmp rmv
	//gpio_toggle_pin_level(LED0);
	
	return;
}