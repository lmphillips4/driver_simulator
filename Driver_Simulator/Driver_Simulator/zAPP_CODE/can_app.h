/*
 * can_app.h
 *
 * Created: 2/13/2019 4:24:21 PM
 *  Author: lphillips_admin
 */ 


#ifndef CAN_APP_H_
#define CAN_APP_H_



#include "atmel_start.h"


#define CAN_TX_APP_ID  0x100
#define CAN_TX_BPP_ID  0x101
#define CAN_TX_STEER_ID  0x102

#define CAN_FILTER_ID_0		0x200
#define CAN_FILTER_ID_1		0x201


struct CANMSG {
	uint32_t id;
	uint8_t data[8];
};

typedef struct CANMSG canMsg;


extern void can_app_init (void);
extern uint8_t can_app_get_msg (canMsg *msgRx, uint16_t waitTime);
extern int32_t can_app_send_msg (uint32_t msgID, uint8_t *dataArray, uint8_t length);
extern int32_t can_app_send_msg_ptr (canMsg msgRx);

//extern can_app_new_message (can_message *);



#endif /* CAN_APP_H_ */