/*
 * pedal_pot.c
 *
 * Created: 2/26/2019 5:54:06 AM
 *  Author: lmphi
 */ 


#include "pedal_pot.h"


static void app_pot_cb (const struct adc_async_descriptor *const descr, const uint8_t channel);
static void bpp_pot_cb (const struct adc_async_descriptor *const descr, const uint8_t channel);

volatile uint16_t appPotVal;
volatile uint16_t bppPotVal;


void pedal_pot_init (void)
{
	adc_async_register_callback(&ADC_APP, 0, ADC_ASYNC_CONVERT_CB, app_pot_cb);
	adc_async_register_callback(&ADC_BPP, 0, ADC_ASYNC_CONVERT_CB, bpp_pot_cb);
	adc_async_enable_channel(&ADC_APP, 0);
	adc_async_enable_channel(&ADC_BPP, 0);
}

uint16_t pedal_pot_get_app (void)
{
	return appPotVal;
}


uint16_t pedal_pot_get_bpp (void)
{
	return bppPotVal;
}



//////////////////////////////////////////////////////////////////////////
//  ADC callbacks

static void app_pot_cb (const struct adc_async_descriptor *const descr, const uint8_t channel)
{
	uint8_t appBuffer[2];
	
	adc_async_read_channel(&ADC_APP, 0, appBuffer, 2);
	appPotVal = 0;
	appPotVal = appBuffer[0];
	appPotVal += appBuffer[1] << 8;
	
	adc_async_start_conversion(&ADC_APP);
}

static void bpp_pot_cb (const struct adc_async_descriptor *const descr, const uint8_t channel)
{
	uint8_t bppBuffer[2];
	
	adc_async_read_channel(&ADC_BPP, 0, bppBuffer, 2);
	bppPotVal = 0;
	bppPotVal = bppBuffer[0];
	bppPotVal += bppBuffer[1] << 8;
	
	adc_async_start_conversion(&ADC_BPP);
}