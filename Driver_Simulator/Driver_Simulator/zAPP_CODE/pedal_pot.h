/*
 * pedal_pot.h
 *
 * Created: 2/26/2019 5:54:19 AM
 *  Author: lmphi
 */ 


#ifndef PEDAL_POT_H_
#define PEDAL_POT_H_



#include "atmel_start.h"


void pedal_pot_init (void);
uint16_t pedal_pot_get_app (void);
uint16_t pedal_pot_get_bpp (void);



#endif /* PEDAL_POT_H_ */