/*
 * stepper_motor.c
 *
 * Created: 2/9/2019 1:35:56 PM
 *  Author: lmphi
 */ 


#include "stepper_motor.h"


#include <hal_gpio.h>
#include <hal_delay.h>




int32_t app_location;  // persistent variable to keep up with where the motor is
int32_t app_destination;
bool app_direction;

int32_t bpp_location;  // persistent variable to keep up with where the motor is
int32_t bpp_destination;
bool bpp_direction;

void pedal_init (void)
{
	app_location = 0;
	app_destination = 0;
	app_direction = APP_DIR_PRESS;
	bpp_location = 0;
	bpp_destination = 0;
	bpp_direction = BPP_DIR_PRESS;
}


//  our inputs are in the range 0-1000  -  (0-100%)*10
void pedal_set_destination_app (int32_t destination)
{
	// convert the percent based input to a step destination
	app_destination = (destination * PEDAL_MAX_POSITION) / 1000;
}

//  our inputs are in the range 0-1000  -  (0-100%)*10
void pedal_set_destination_bpp (int32_t destination)
{
	// convert the percent based input to a step destination
	bpp_destination = (destination * PEDAL_MAX_POSITION) / 1000;
}


void pedal_pulse_handler (void)
{
	////
	// Accelerator Pedal - check if we need to move and which direction
	
	//  if we are not at the destination enter
	if (app_location != app_destination) {
		
		// if the pedal is currently past the destination, we need to release the pedal
		if (app_location > app_destination) {
			if (app_direction != APP_DIR_RELEASE) {
				app_direction = APP_DIR_RELEASE;
				gpio_set_pin_level(APP_DIR, app_direction);
				delay_us(5); // this is to give time for the signal to process in motor controller
			}
			app_location--;
		}
		
		// if the pedal is currently before the destination, we need to press the pedal
		else if (app_location < app_destination) {
			if (app_direction != APP_DIR_PRESS) {
				app_direction = APP_DIR_PRESS;
				gpio_set_pin_level(APP_DIR, app_direction);
				delay_us(5); // this is to give time for the signal to process in motor controller
			}
			app_location++;
		}
		
		// the direction has been set, and we are not at the destination so start a pulse
		gpio_set_pin_level(APP_PULSE, PEDAL_PULSE_LOW);
	}
	
	
	
	////
	// Brake Pedal - check if we need to move and which direction
	
	if (bpp_location != bpp_destination) {
		if (bpp_location > bpp_destination) {
			if (bpp_direction != BPP_DIR_RELEASE) {
				bpp_direction = BPP_DIR_RELEASE;
				gpio_set_pin_level(BPP_DIR, bpp_direction);
				delay_us(5); // this is to give time for the signal to process in motorr contrroller
			}
			
			bpp_location--;
		}
		
		else if (bpp_location < bpp_destination) {
			if (bpp_direction != BPP_DIR_PRESS) {
				bpp_direction = BPP_DIR_PRESS;
				gpio_set_pin_level(BPP_DIR, bpp_direction);
				delay_us(5); // this is to give time for the signal to process in motorr contrroller
			}
			
			bpp_location++;
		}
		
		gpio_set_pin_level(BPP_PULSE, PEDAL_PULSE_LOW);
	}
	
	
	delay_us(PEDAL_PULSE_WIDTH_US);
	
	
	// idle pin state is high because it's better to not activate the switch on motor controller
	gpio_set_pin_level(APP_PULSE, PEDAL_PULSE_HIGH);
	gpio_set_pin_level(BPP_PULSE, PEDAL_PULSE_HIGH);
	
	delay_us(PEDAL_START_SPEED - PEDAL_PULSE_WIDTH_US);
}