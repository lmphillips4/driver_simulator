/*
 * stepper_motor.h
 *
 * Created: 2/9/2019 1:36:07 PM
 *  Author: lmphi
 */ 


#ifndef STEPPER_MOTOR_H_
#define STEPPER_MOTOR_H_

#include "atmel_start.h"



#define PEDAL_PULSE_HIGH	true
#define PEDAL_PULSE_LOW		false

#define APP_DIR_PRESS		true
#define APP_DIR_RELEASE		false

#define BPP_DIR_PRESS		false
#define BPP_DIR_RELEASE		true



#define PEDAL_PULSE_WIDTH_US  20		// pulse width for commanding a step - value in micro seconds  -  this does not effect speed


#define PEDAL_START_SPEED		300   //  period for sending a pulse signal to tell motor to move one step  -  this controls speed

#define PEDAL_MAX_POSITION	580		// this is where the motor will limit it's position  -  if you change the micro stepes on the motor drivers, this will need to change



extern void pedal_init (void);
extern void pedal_pulse_handler (void);
extern void pedal_set_destination_bpp (int32_t destination);
extern void pedal_set_destination_app (int32_t destination);



#endif /* STEPPER_MOTOR_H_ */